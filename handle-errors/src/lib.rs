
use warp::{reject::Reject, Rejection, Reply, hyper::StatusCode, cors::CorsForbidden, body::BodyDeserializeError};

/// our own, application specific error type(Enum), it is used to 
/// define and react to posible error situations in the project
#[derive(Debug)]
pub enum Error {
    /// Some Integer cannot be parsed from a non-integer type
    ParseError(std::num::ParseIntError),
    /// The pagination end parameter is smaller than the pagination start parameter
    EndSmallerThanStart,
    /// a parameter is missing, the String payload holds additional information that 
    /// could be displayed in response to a request
    MissingParameters(String),
    /// there is no question for the given id in the database
    QuestonNotFound,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {

        match self {
            Error::ParseError(ref err) => {
                write!(f, "Cannot pase parameter: {}", err)
            },
            Error::EndSmallerThanStart => write!(f, "Param end is smaller than start"), 
            Error::MissingParameters(desc) => write!(f, "{}", &desc),
            Error::QuestonNotFound => write!(f, "Question not found"),

        }
    }
}

impl Reject for Error{}


/// transforms error types to warp::Rejection
/// this function can be used in a warp filter chain to handle the error types defined in this crate
pub async fn return_error(r: Rejection) -> Result<impl Reply, Rejection> {
    println!("{:?}", r);

    if let Some(error) = r.find::<Error>() {
        Ok(warp::reply::with_status(
        error.to_string(),
        StatusCode::RANGE_NOT_SATISFIABLE,
        ))
    } else if let Some(error) = r.find::<CorsForbidden>() {
        // actually never called, because the cors middleware is not called properly in the filter chain
        Ok(warp::reply::with_status(
            error.to_string(),
            StatusCode::FORBIDDEN,
        ))
    } else if let Some(error) = r.find::<BodyDeserializeError>() {
        Ok(warp::reply::with_status(
            error.to_string(),
            StatusCode::UNPROCESSABLE_ENTITY,
            ))
    } else {
        Ok(warp::reply::with_status(
            "Route not found".to_string(),
            StatusCode::NOT_FOUND,
        ))
    }
}



