#![warn(clippy::all)]
use handle_errors::return_error;
use routes::answer;
use routes::question;
use warp::{http::Method, Filter};

mod routes;
mod store;
mod types;

#[macro_use]
extern crate log;

#[tokio::main]
async fn main() {
    env_logger::init();
    info!("starting up");

    let store = store::Store::new();
    let store_filter = warp::any().map(move || store.clone());

    //cors not working right now
    let cors = warp::cors()
        .allow_any_origin()
        //.allow_origin("https://spasshpo.de")
        .allow_headers(vec![
            "User-Agent",
            "Sec-Fetch-Mode",
            "Referer",
            "Origin",
            "Access-Control-Request-Method",
            "Access-Control-Request-Headers",
        ])
        .allow_methods(&[
            Method::PUT,
            Method::DELETE,
            Method::POST,
            Method::GET,
            Method::OPTIONS,
        ])
        .build();

    let get_questions = warp::get()
        .and(warp::path("questions"))
        .and(warp::path::end())
        .and(warp::query()) // filter query params
        .and(store_filter.clone())
        .and_then(question::get_questions);

    let get_question = warp::get()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(store_filter.clone())
        .and_then(question::get_question);

    let add_question = warp::post()
        .and(warp::path("questions"))
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::json())
        .and_then(question::add_question);

    let update_question = warp::put()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::json())
        .and_then(question::update_question);

    let remove_question = warp::delete()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(store_filter.clone())
        .and_then(question::remove_question);

    let get_answers_for_question = warp::get()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path("answers"))
        .and(warp::path::end())
        .and(store_filter.clone())
        .and_then(answer::get_answers_for_question);

    let add_answer = warp::post()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path("answers"))
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::form())
        .and_then(answer::add_answer);

    // handle multiple routes with .or()
    let routes = get_questions
        .or(get_question)
        .or(add_question)
        .or(update_question)
        .or(remove_question)
        .or(add_answer)
        .or(get_answers_for_question)
        .with(cors)
        .with(warp::log("defaultLogger"))
        .recover(return_error);

    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}
