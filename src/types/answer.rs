use super::question::QuestionId;
use serde::{Deserialize, Serialize};

/// Tuple struct containing the id of an Answer struct
///
/// Example:
/// ```
/// let answer_id = AnswerId(String::from("1351-wet31-wer235z5"));
/// ```
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct AnswerId(
    /// Actual id of the answer, stored as String
    pub String,
);

/// Struct that represents an Answer to a question
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Answer {
    /// id of this answer
    pub id: AnswerId,
    /// content of the answer, the text that was submitted by the user
    pub content: String,
    /// id of the question answered
    pub question_id: QuestionId,
}
