use std::{
    io::{Error, ErrorKind},
    str::FromStr,
};

use serde::{Deserialize, Serialize};

/// Represents the ID of a Question struct
#[derive(Debug, Deserialize, Serialize, Eq, Hash, PartialEq, Clone)]
pub struct QuestionId(
    /// Id of the question, stored as String
    pub String,
);

impl FromStr for QuestionId {
    type Err = std::io::Error;
    /// this is defined as shorthand, so that the String id does not have to be constructed manually
    ///
    /// usage:
    /// ```
    /// let id = QuestionId::from_str("asd");
    /// assert_eq!(id.0, String::from("asd"));
    /// ```
    fn from_str(id: &str) -> Result<Self, Self::Err> {
        match id.is_empty() {
            false => Ok(QuestionId(id.to_string())),
            true => Err(Error::new(ErrorKind::InvalidInput, "No id provided")),
        }
    }
}

impl Default for QuestionId {
    fn default() -> Self {
        return QuestionId(String::from("?"));
    }
}
/// represents a question a user of this application may have and submit to the database
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Question {
    // allows for "empty" question id ("?")
    // or maybe rather use Some(QuestionId), this might be necessary later
    // anyways, because the db will assingn ids
    /// the id of the question
    #[serde(default)]
    pub id: QuestionId,
    /// A headline or short summary of the question
    pub title: String,
    /// The actual, maybe lengthy content of the question where the question
    /// is properly asked and explained
    pub content: String,
    /// an optional list of tags that is used to categorize questions
    pub tags: Option<Vec<String>>,
}

impl Question {
    fn _new(id: QuestionId, title: String, content: String, tags: Option<Vec<String>>) -> Self {
        Question {
            id,
            title,
            content,
            tags,
        }
    }
}
