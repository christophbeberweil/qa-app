use std::collections::HashMap;

use crate::store::Store;
use crate::types::pagination::extract_pagination;
use crate::types::question::{Question, QuestionId};
use handle_errors::Error;
use uuid::Uuid;
use warp::http::StatusCode;

pub async fn get_questions(
    params: HashMap<String, String>,
    store: Store,
) -> Result<impl warp::Reply, warp::Rejection> {
    if !params.is_empty() {
        let pagination = extract_pagination(params)?;

        let res: Vec<Question> = store.questions.read().await.values().cloned().collect();
        let mut result = &res[..];
        if pagination.start <= res.len() && pagination.end <= res.len() {
            result = &result[pagination.start..pagination.end];
            println!("{:?}", res);
        }
        Ok(warp::reply::json(&result))
    } else {
        // convert a hash map of <a, b> to a vec of b by ignoring a:
        let res: Vec<Question> = store.questions.read().await.values().cloned().collect();
        Ok(warp::reply::json(&res))
    }
}

pub async fn get_question(id: String, store: Store) -> Result<impl warp::Reply, warp::Rejection> {
    let question_id = QuestionId(id.to_owned());

    match store.questions.read().await.get(&question_id) {
        Some(res) => Ok(warp::reply::json(&res)),
        None => Err(warp::reject::custom(Error::QuestonNotFound)),
    }
}

pub async fn add_question(
    store: Store,
    mut question: Question,
) -> Result<impl warp::Reply, warp::Rejection> {
    let question_id = QuestionId(Uuid::new_v4().to_string());
    question.id = question_id.clone();

    store
        .questions
        .write()
        .await
        .insert(question_id, question.clone());

    Ok(warp::reply::json(&question))
}

pub async fn update_question(
    id: String,
    store: Store,
    question: Question,
) -> Result<impl warp::Reply, warp::Rejection> {
    match store.questions.write().await.get_mut(&QuestionId(id)) {
        Some(q) => *q = question,
        None => return Err(warp::reject::custom(Error::QuestonNotFound)),
    }

    Ok(warp::reply::with_status("Question Updated", StatusCode::OK))
}
pub async fn remove_question(
    id: String,
    store: Store,
) -> Result<impl warp::Reply, warp::Rejection> {
    match store.questions.write().await.remove(&QuestionId(id)) {
        Some(_q) => {}
        None => return Err(warp::reject::custom(Error::QuestonNotFound)),
    };

    Ok(warp::reply::with_status("Question Removed", StatusCode::OK))
}
