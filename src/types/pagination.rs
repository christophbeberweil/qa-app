use std::collections::HashMap;

use handle_errors::Error;

/// Pagination struct that is getting extracted
/// from quers parameters
///
/// Create a new pagination struct for instance like this:
/// ```
/// let pagination = Pagination(start: 1, end 10);
/// ```
#[derive(Debug)]
pub struct Pagination {
    /// The index of the first item that has to be returned
    pub start: usize,
    /// the index of the last item that has to be returned
    pub end: usize,
}

/// Extract query parameters from the /questions route
/// # Example query
/// GET requests to this route can have a pagination attached
/// so we just return the questions we need
/// `/qustions?start=1&end=10`
/// # Example usage
/// ```rust
/// let mut query = HashMap::new();
/// query.insert("start".to_string(), "1".to_string());
/// query.insert("end".to_string(), "10".to_string());
/// let p = types::pagination::extract_pagination(query).unwrap();
/// assert_eq!(p.start, 1);
/// assert_eq!(p.end, 10);
/// ```
pub fn extract_pagination(params: HashMap<String, String>) -> Result<Pagination, Error> {
    // could be improved in the future
    if params.contains_key("start") && params.contains_key("end") {
        // try to convert the parameters
        // todo: remove unwrap()
        let start = params
            .get("start")
            .unwrap()
            .parse::<usize>()
            .map_err(Error::ParseError)?;
        let end = params
            .get("end")
            .unwrap()
            .parse::<usize>()
            .map_err(Error::ParseError)?;

        if start <= end {
            return Ok(Pagination { start, end });
        } else {
            // return an Error if the pagination parameters mismatch
            return Err(Error::EndSmallerThanStart);
        }
    }
    // return an Error if only one of the parmeters is present
    Err(Error::MissingParameters(String::from(
        "the query parameters start and end are required together",
    )))
}
