# QA App
A small REST api server to handle questions and their answers. Written in Rust.

## Developing

### Formatting

```
cargo fmt
```

### Linting 
```
cargo clippy
```

apply linting:
```
cargo clippy --fix --bin "qa_app"
```

### Build Documentation
```
cargo doc --open
```

### Run
```
RUST_LOG=info cargo run 
```