use handle_errors::Error;
use std::collections::HashMap;
use uuid::Uuid;

use crate::{
    store::Store,
    types::{
        answer::{Answer, AnswerId},
        question::QuestionId,
    },
};

pub async fn get_answers_for_question(
    question_id: String,
    store: Store,
) -> Result<impl warp::Reply, warp::Rejection> {
    let all_answers: Vec<Answer> = store
        .answers
        .read()
        .await
        .values()
        .cloned()
        .filter(|element| element.question_id == QuestionId(question_id.to_owned()))
        .collect();

    Ok(warp::reply::json(&all_answers))
}

pub async fn add_answer(
    question_id: String,
    store: Store,
    params: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    let Some(content) = params.get("content") else {
        return Err(warp::reject::custom(Error::MissingParameters(String::from("content is required"))));
    };

    let question_id = QuestionId(question_id.to_owned());

    // check if the question exists,
    // let else allows us to keep the happy path in this context
    let Some(_question) = store.questions.read().await.get(&question_id) else {
        return Err(warp::reject::custom(Error::QuestonNotFound));
    };

    let answer = Answer {
        id: AnswerId(Uuid::new_v4().to_string()),
        content: content.to_owned(),
        question_id,
    };

    store
        .answers
        .write()
        .await
        .insert(answer.id.clone(), answer.clone());

    Ok(warp::reply::json(&answer))
}
