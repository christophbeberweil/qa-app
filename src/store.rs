use crate::types::{
    answer::{Answer, AnswerId},
    question::{Question, QuestionId},
};
use std::{collections::HashMap, sync::Arc};
use tokio::sync::RwLock;

#[derive(Clone)]
pub struct Store {
    // Arc duplicates pointers to the data and increments and decrements their count
    // RwLock allows only one concurrent write but multiple concurrent reads
    pub questions: Arc<RwLock<HashMap<QuestionId, Question>>>,
    pub answers: Arc<RwLock<HashMap<AnswerId, Answer>>>,
}

impl Store {
    pub fn new() -> Self {
        Store {
            questions: Arc::new(RwLock::new(Self::init_questions())),
            answers: Arc::new(RwLock::new(Self::init_answers())),
        }
    }

    fn init_questions() -> HashMap<QuestionId, Question> {
        let file = include_str!("../questions.json");
        serde_json::from_str(file).expect("can't read questions.json")
    }

    fn init_answers() -> HashMap<AnswerId, Answer> {
        let file = include_str!("../answers.json");
        serde_json::from_str(file).expect("can't read answers.json")
    }
}
